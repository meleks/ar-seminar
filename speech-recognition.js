var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent


var recognition = new SpeechRecognition();
recognition.continuous = true;
recognition.lang = "";
recognition.interimResults = false;
recognition.maxAlternatives = 1;

recognition.onerror = function (event) {
    if (event.error !== "no-speech") {
        console.log('Error occurred in recognition: ' + event.error);
    }
}

recognition.onend = function () {
    console.log("Speech has stopped, restarting Speech Recognition...");
    recognition.start();
};

recognition.onresult = function (event) {
    let utterance = event.results[event.results.length - 1][0].transcript.trim();
    let results = utterance.split(" ");
    for (let i = 0; i < results.length; i++) {
        results[i] = results[i].toLowerCase();
    }
    console.log(utterance);
    get_intent(utterance);
}

var myTimeout;

function myTimer() {
    window.speechSynthesis.pause();
    window.speechSynthesis.resume();
    myTimeout = setTimeout(myTimer, 10000);
}


stop_recognition = function () {
    recognition.onend = function () {
    };
    recognition.stop();
}

start_recognition = function () {
    recognition.onend = function () {
        console.log("Speech has stopped, restarting Speech Recognition...");
        recognition.start();
    };
    recognition.start();
}


function say_utterace(utterance, lang = "de-de", onend = function () {
}) {
    let msg = new SpeechSynthesisUtterance();
    msg.onstart = function () {
        stop_recognition();
        recognition.stop();
    }
    msg.onend = function () {
        clearTimeout(myTimeout);
        start_recognition();
        onend();
    };
    msg.text = utterance;
    msg.lang = lang;
    msg.onerror = function (event) {
        console.log('An error has occurred with the speech synthesis: ' + event.error);
    }
    window.speechSynthesis.cancel();
    myTimeout = setTimeout(myTimer, 10000);
    window.speechSynthesis.speak(msg);
}

