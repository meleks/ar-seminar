function get_intent(utterance) {
    const uri = 'https://api.wit.ai/message?v=20210603&q=' + utterance;
    const auth = "Bearer " + "VIFBQVR37AKPQFKASQ6O4GVPVPICAJG5";

    $.ajax(uri, {
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", auth);
        },
        success: function (data, status, xhr) {
            console.log(data);
            handle_intent(data);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.error(errorMessage);
            console.error(jqXhr.responseText);
        }
    });
}

function handle_intent(data) {
    let name, confidence, entities, subject;

    try {
        name = data["intents"][0]["name"];
        confidence = data["intents"][0]["confidence"];
        entities = data["entities"];
    } catch (error) {
        if (error instanceof TypeError) {
            console.debug("Kein Intent gefunden");
            //If there is no intent, the process cannot be continued
            return
        } else {
            console.error(error);
            return
        }
    }
    if (entities["subjekt:subjekt"]) {
        subject = entities["subjekt:subjekt"][0]["value"];
    }
    intent_fulfillment();

    function intent_fulfillment() {
        if (confidence > 0.6) {
            switch (name) {
                case "wechsel":
                    console.log("change to: " + subject)
                    say_utterace("Wechsel zu" + subject)
                    switch (subject) {
                        case "mensch":
                            reset();
                            loadHuman();
                            break;
                        case "clippy":
                            if (entities["subjekt:subjekt"][1]) {
                                subject = entities["subjekt:subjekt"][1]["value"];
                                intent_fulfillment();
                                return;
                            } else {
                                reset();
                                loadClippy();
                            }
                            break;
                        default:
                            console.debug("No subject found")
                            break;
                    }

                    break;
                case "show":
                    console.log("show: ", subject)
                    say_utterace("Zeige" + subject)
                    show_organ(subject);
                    break;
                default:
                    console.debug("No matching intent")
                    break;
            }
        }
    }
}